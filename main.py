# beginning of main3.py
from flask import render_template
#from models import  app, db, Book
from create_db import app, db, Book, create_books
import json


#books = [{'title': 'Software Engineering', 'id': '1'}, {'title':'Algorithm Design', 'id':'2'},{'title':'Python', 'id':'3'}]
# beginning of models.py
# note that at this point you should have created "bookdb" database (see install_postgres.txt).
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:postgres@localhost:5432/project4')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

class Book(db.Model):
	__tablename__ = 'book'
	title = db.Column(db.String(80), nullable = False)
	id = db.Column(db.Integer, primary_key = True)

db.drop_all()
db.create_all()
# End of models.py

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def create_books():
    book = load_json('books.json')

    for oneBook in book['Books']:
        title = oneBook['title']
        id = oneBook['id']
		
        newBook = Book(title = title, id = id)
        
        # After I create the book, I can then add it to my session. 
        db.session.add(newBook)
        # commit the session to my DB.
        db.session.commit()
		
create_books()
# end of create_db.py




@app.route('/')
def index():
	return render_template('hello.html')
	
@app.route('/book2/')
def book():
	books = db.session.query(Book).all()
	return render_template('book2.html', books = books)
	
if __name__ == "__main__":
	app.run()
# end of main3.py
